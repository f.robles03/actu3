package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


@SpringBootApplication

public class DemoApplication {
    //String filePath="Users/ferna/Desktop/demo/uf.csv";
    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
        double opc1 = 0;
        double opc2 = 0;
		int contador=0;
		double opc3 = 0;
		double opc4 = 0;
		int contador2=0;
		double opc5 = 0;
		double opc6 = 0;
		int contador3=0;

        try {
            BufferedReader reader = new BufferedReader(new FileReader("uf.csv"));
            String line = null;
					while ((line = reader.readLine()) != null) {
							/**
							 * pregunta 1
							 */
							if (line.contains("/8/99")) {//34
								int tamano = line.length();
								String res = line.substring(8, tamano - 1);
								String res2 = res.replaceFirst("(?:,)+", "");


								if ((int) (res2.charAt(0)) == 34) {
									res2 = res2.substring(1);
									String res3 = res2.replaceFirst("(?:,)+", "");
									double str1 = Double.parseDouble(res3);
									//System.out.println(str1);
									opc1 = str1+opc1;
								} else {
									String res3 = res2.replaceFirst("(?:,)+", "");
									double str2 = Double.parseDouble(res3);
								  //  System.out.println(str2);
									opc2 = str2+opc2;
								}

								contador++;
							}


								/**
								 * pregunta 2
								 */


								if (line.substring(3).contains("/10")) {//34
									int tamano = line.length();
									String res = line.substring(8, tamano - 1);
									String res2 = res.replaceFirst("(?:,)+", "");

									if ((int) (res2.charAt(0)) == 34) {
										res2 = res2.substring(1);
										String res3 = res2.replaceFirst("(?:,)+", "");
										double str1 = Double.parseDouble(res3);
										//System.out.println(str1);
										opc3 = str1+opc3;
									} else {
										String res3 = res2.replaceFirst("(?:,)+", "");
										double str2 = Double.parseDouble(res3);
										//  System.out.println(str2);
										opc4 = str2+opc4;
									}

									contador2++;
								}

						System.out.print("");
									/**
									 * pregunta 3
									 */
									if (line.contains("1/1/98")&& line.contains("14,097.38")) {//34
										int tamano = line.length();
										String res = line.substring(8, tamano - 1);
										String res2 = res.replaceFirst("(?:,)+", "");

										if ((int) (res2.charAt(0)) == 34) {
											res2 = res2.substring(1);
											String res3 = res2.replaceFirst("(?:,)+", "");
											double str1 = Double.parseDouble(res3);
											//System.out.println(str1);
											opc5 = str1;
										} else {
											String res3 = res2.replaceFirst("(?:,)+", "");
											double str2 = Double.parseDouble(res3);
											//  System.out.println(str2);
											opc5 = str2;
										}
									}
							if (line.contains("1/1/00") && line.contains("15,067.93") ) {//34
								int tamano = line.length();
								String res = line.substring(8, tamano - 1);
								String res2 = res.replaceFirst("(?:,)+", "");

								if ((int) (res2.charAt(0)) == 34) {
									res2 = res2.substring(1);
									String res3 = res2.replaceFirst("(?:,)+", "");
									double str1 = Double.parseDouble(res3);
									//System.out.println(str1);
									opc6 = str1;
								} else {
									String res3 = res2.replaceFirst("(?:,)+", "");
									double str2 = Double.parseDouble(res3);
									//  System.out.println(str2);
									opc6 = str2;
								}

							}





					}
			System.out.println("Pregunta 1:");
			System.out.println("El promedio del mes de agosto del año 1999 es de: "+((opc1+opc2)/contador));
			System.out.println("");
			System.out.println("Pregunta 2:");
			System.out.println("El el promedio de la uf de todos los meses correspondientes al año 2010 es de: "+((opc3+opc4)/contador2));
			System.out.println("");
			System.out.println("Pregunta 3:");
			System.out.println("uf del 1/1/98 es: "+ opc5);
			System.out.println("uf del 1/1/00 es: "+ opc6);
			System.out.println("El porcentaje de variación entre el 01/01/1998 y el 01/01/2000 es: "+( (opc6-opc5)/opc5)*100);
			System.out.println("");

		} catch (IOException e) {
            e.printStackTrace();
        }


    }



}
